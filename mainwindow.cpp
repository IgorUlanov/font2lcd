#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>
#include <QTextCodec>
#include <QFrame>
#include <QMenu>
#include <QAction>
//#include <QDebug>
#include "qdir.h"
#include <QFormLayout>
#include <QProgressDialog>
#include <QStandardPaths>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // считывание таблицы symbol
    QFile f_sym("symbol.csv");
    f_sym.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream sym(&f_sym);
    sym.setCodec(QTextCodec::codecForName("UTF-8"));
    int i=0;
    while (!sym.atEnd())
     {
         QString str = sym.readLine();
         QList<QString> list=str.split(QRegExp("\\b(;)\\b"));
         for (int n=0;n<list.size();n++) utf[i++]=list.value(n).toUInt(0,10);
     }
     f_sym.close();
    format=QImage::Format_RGB32;
    symbol.rotate=0;
	ui->widget->installEventFilter(this);
    fnt_d=new QFontDialog(parent);
    connect(fnt_d, SIGNAL(currentFontChanged(const QFont &)),
                 this, SLOT(fontUpdate(const QFont &)));
    fnt=this->font();
    fnt.setPointSize(26);
    symbol.pen=Qt::black;
    symbol.brush=Qt::white;
    symbol.code='W';
    Calc();
    symbol.top=0;
    symbol.bottom=symbol.height;
    ui->table->setFont(QFont(fnt.family(),10,QFont::Normal,false));
    fillTable();
    ui->sliderBottom->setRange(1-(symbol.height),0);
    ui->sliderTop->setRange(0,symbol.height-1);
    showFont();
	home_path=QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first()+QDir::separator();
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::fillTable(void){
    for (int y=0;y<16;y++)
        for (int x=0;x<16;x++){
            int n=y*16+x;
            QTableWidgetItem *newitem = new QTableWidgetItem(QChar(utf[n]));
            newitem->setTextAlignment(Qt::AlignCenter);
            newitem->setStatusTip("yes");
            newitem->setForeground(Qt::black);
            ui->table->setItem(y,x,newitem);
        }
}

void MainWindow::on_table_cellClicked(int row, int column){
    symbol.code=utf[row*16+column];
    ui->widget->update();
}

void MainWindow::on_push_pen_clicked(){
    QColor c=QColorDialog::getColor(symbol.pen, this);
    if (c.isValid()) symbol.pen = c;
}

void MainWindow::on_push_brush_clicked(){
    QColor c=QColorDialog::getColor(symbol.brush, this);
    if (c.isValid()) symbol.brush = c;
}

void MainWindow::appendSymbol(void){
	// добавление символа в массив
	paintSymbol();
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	int af=symbol.width;
    int bf=symbol.bottom-symbol.top;
	if (set->value("scan_lcd").toBool()){
        af=bf;
        bf=symbol.width;
    }
    QRgb rgb=0;
    for (int a=0;a<af;a++){
        for (int b=0;b<bf;b++){
			if (set->value("scan_lcd").toBool()) rgb=img->pixel(b,a+symbol.top);
            else rgb=img->pixel(a,b+symbol.top);
                quint16 B =  (qBlue(rgb)  >> 3)        & 0x001F;
                quint16 G = ((qGreen(rgb) >> 2) <<  5) & 0x07E0; // not <
                quint16 R = ((qRed(rgb)   >> 3) << 11) & 0xF800; // not <
                quint16 c=R | G | B;
                font_.append(c);
            }
        }
	if (set->value("scan_lcd").toBool()) bias+=bf; else bias+=af;
    map_.append(bias);
    width_.append(symbol.width);
}

void MainWindow::saveSymbol(int r, int c){
    // запись одного символа
    paintSymbol();
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	first << QString("\t0x%1,0x%2, // 0x%3 \"%4\"\n")
           .arg(bias,4,16,QLatin1Char('0'))
           .arg(symbol.width,4,16,QLatin1Char('0'))
           .arg(r*16+c,2,16,QLatin1Char('0'))
           .arg(QChar(symbol.code));
    second << QString("\n\t// symbol 0x%1 \"%2\" ").arg(r*16+c,2,16,QLatin1Char('0')).arg(QChar(symbol.code));
    int af=symbol.width;
    int bf=symbol.bottom-symbol.top;
	if (set->value("scan_lcd").toBool()){
        af=bf;
        bf=symbol.width;
    }
    QRgb rgb=0;
    for (int a=0;a<af;a++)
    {
        for (int b=0;b<bf;b++)
            {
			if (set->value("scan_lcd").toBool()) rgb=img->pixel(b,a+symbol.top);
            else rgb=img->pixel(a,b+symbol.top);
                quint16 B =  (qBlue(rgb)  >> 3)        & 0x001F;
                quint16 G = ((qGreen(rgb) >> 2) <<  5) & 0x07E0; // not <
                quint16 R = ((qRed(rgb)   >> 3) << 11) & 0xF800; // not <
                quint16 c=R | G | B;
                if (((a*(bf)+b)%8)==0) second << "\n\t";
                second << QString("0x%1,").arg(c,4,16,QLatin1Char('0'));
            }
        }
	if (set->value("scan_lcd").toBool()) bias+=bf; else bias+=af;
}

void MainWindow::on_push_save_clicked(){
    // создается таблица адресации символов map_* и сам массив символов font_*.
    // создаются в одном файле. Общее название - имя шрифта+размер.
    // также создается заголовочный файл с объявлением этих массивов.
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	QString all=nameFont();
	QProgressDialog *progress=new QProgressDialog(tr("save files: %1").arg(home_path+"font_"+all+".c"), "Abort Save", 0, 255,this);
	progress->setWindowModality(Qt::WindowModal);
	progress->show();
	first.clear();
    second.clear();
    bias=0;
    int back=symbol.code;
    font_.clear();
    map_.clear();
    width_.clear();
    // последовательно считывается таблица. Для выбранных символов формируется массив,
    // для не выбранных записывается 0 в таблицу адресов.
    for (int r=0;r<16;r++) for (int c=0;c<16;c++){
		progress->setValue(r*16+c);
		QCoreApplication::processEvents();
        symbol.code=utf[r*16+c];
        if (symbol.code==0x20) appendSymbol();
        else if (ui->table->item(r,c)->statusTip()=="yes") appendSymbol();
            else{
            width_.append(0);
            map_.append(bias);
        }
    }
    if (set->value("arc").toInt()==Qt::Checked){
        // сжать шрифт
        fillDictionary();
        dest.clear();
        int s=0;
        for (int i=0;i<256;i++){
            int f=map_[i]*(symbol.bottom-symbol.top);
            if (f>s) fillArc(s,f-s);
            map_[i]=dest.count();
            s=f;
        }
    }
    QFile f_c(home_path+"font_"+all+".c");
    QFile f_h(home_path+"font_"+all+".h");
    f_c.open(QIODevice::WriteOnly | QIODevice::Text);
    f_h.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream c(&f_c);
    QTextStream h(&f_h);
    c.setCodec(QTextCodec::codecForName("UTF-8"));
    h.setCodec(QTextCodec::codecForName("UTF-8"));
    int n;
    if (set->value("arc").toInt()==Qt::Checked) n=dest.count(); else n=font_.count()*2;
    QString str=tr("// the size of an array of characters = %1 Kb%2")
            .arg(n/1024)
            .arg("\n");
    c << str;
    h << str;
	QString style=" ";
	if (fnt.bold()) style+="bold ";
	if (fnt.italic()) style+="italic ";
	str="// '"+fnt.family()+"' "+QString::number(fnt.pointSize())+"p"+style+"\n";
	c << str;
	h << str;
	str=QString("// pen(rgb) %1,%2,%3 brush(rgb) %4,%5,%6\n\n")
			.arg(QString::number(symbol.pen.red()))
			.arg(QString::number(symbol.pen.green()))
			.arg(QString::number(symbol.pen.blue()))
			.arg(QString::number(symbol.brush.red()))
			.arg(QString::number(symbol.brush.green()))
			.arg(QString::number(symbol.brush.blue()));
	c << str;
	h << str;

	// include
	c << set->value("include").toString() << "\n";
    // const height
	str=set->value("type16").toString()+" height_"+all;
    h << "extern " << str << ";\n";
	str+=" "+set->value("attr").toString()+"="+QString::number(symbol.bottom-symbol.top);
    c << str << ";\n";
    // const map_ []
	str=set->value("type16").toString()+" map_"+all+"[]";
    h << "extern " << str << ";\n";
	str+=" "+set->value("attr").toString()+"={";
    c << str;
    str="";
    for (int i=0;i<map_.size();i++){
        if ((i%8)==0){
            c<<" // "<<str<<"\n\t";
            str="";
        }
        c << QString("0x%1,").arg(map_[i],4,16,QLatin1Char('0'));
        if (width_[i]!=0) str+=QChar(utf[i]); else str+=QChar(utf[0x20]);
    }
    c << " // " << str << "\n\t};\n\n";
    // const font_ []
    if (set->value("arc").toInt()==Qt::Checked){
        // сжатый шрифт
		str=set->value("type8").toString()+" font_"+all+"[]";
        h << "extern " << str << ";\n";
		str+=" "+set->value("attr").toString()+"={\n\t// dictionary";
        c << str;
        // словарь
        for (int i=0;i<DICT;i++){
            if ((i%8)==0) c<<"\n\t";
            c << QString("0x%1,0x%2,")
				 .arg((quint8)dictionary[i],2,16,QLatin1Char('0'))
				 .arg((quint8)(dictionary[i]>>8),2,16,QLatin1Char('0'));
        }
		if (set->value("width").toBool()){
            c<<"\n\t// table symbol widths";
            // таблица со значениями ширины символа
			for (qint32 i=0;i<width_.count();i++){
                if ((i%16)==0) c<<"\n\t";
                c << QString("0x%1,").arg(width_[i],2,16,QLatin1Char('0'));
            }
        }
        c<<"\n\t// table symbols";
        // сам шрифт
		for (qint32 i=0;i<dest.count();i++){
            if ((i%16)==0) c<<"\n\t";
            c << QString("0x%1,").arg(dest[i],2,16,QLatin1Char('0'));
        }
    }
    else{
        // не сжатый шрифт
		str=set->value("type16").toString()+" font_"+all+"[]";
        h << "extern " << str << ";\n";
		str+=" "+set->value("attr").toString()+"={";
        c << str;
        for (int i=0;i<font_.size();i++){
            if ((i%8)==0) c<<"\n\t";
            c << QString("0x%1,").arg(font_[i],4,16,QLatin1Char('0'));
        }
    }
    c<<"\n\t};\n";
    f_c.close();
    f_h.close();
    symbol.code=back;
}

void MainWindow::on_table_customContextMenuRequested(const QPoint &pos){
    QAction* actionUse = new QAction(tr("use simbol(s)"), this);
    QAction* actionNoUse = new QAction(tr("no use symbol(s)"), this);
    connect(actionUse, SIGNAL(triggered()), this, SLOT(useSymbols()));
    connect(actionNoUse, SIGNAL(triggered()), this, SLOT(nouseSymbols()));
    QMenu *contextMenu = new QMenu(this);
    contextMenu->addAction(actionUse);
    contextMenu->addAction(actionNoUse);
    contextMenu->exec(QCursor::pos());
}

void MainWindow::fontUpdate(const QFont & font){
    fnt=font;
	QFont ft=font;
    ft.setPointSize(10);
    ui->table->setFont(ft);//QFont(fnt.family(),10,QFont::Normal,false));
    Calc();
    symbol.top=0;
    symbol.bottom=symbol.height;
    ui->sliderBottom->setRange(1-symbol.height,0);
    ui->sliderTop->setRange(0,symbol.height-1);
    ui->sliderBottom->setValue(0);
    ui->sliderTop->setValue(0);
    showFont();
    checkBorderline();
}

void MainWindow::useSymbols(void){
    for (int r=0;r<ui->table->rowCount();r++)
        for (int c=0;c<ui->table->columnCount();c++)
        {
            if(ui->table->item(r,c)->isSelected())
            {
                // использовать символы
                ui->table->item(r,c)->setStatusTip("yes");
                ui->table->item(r,c)->setForeground(Qt::black);
             }
        }
    showFont();
}

void MainWindow::nouseSymbols(void){
    for (int r=0;r<ui->table->rowCount();r++)
        for (int c=0;c<ui->table->columnCount();c++)
        {
            if(ui->table->item(r,c)->isSelected())
            {
                // не использовать символы
                ui->table->item(r,c)->setStatusTip("no");
                ui->table->item(r,c)->setForeground(Qt::gray);
             }
        }
    showFont();
}

void  MainWindow::showFont(void){
	// вывод информации о шрифте
    QFontMetrics fm(fnt);
    QString str="'"+fnt.family()+"' "+QString::number(fnt.pointSize());
    if (fnt.bold()) str+=" bold";
    if (fnt.italic()) str+=" italic";
    int n=0;
    for (int r=0;r<ui->table->rowCount();r++)
        for (int c=0;c<ui->table->columnCount();c++){
        if (ui->table->item(r,c)->statusTip()=="yes"){
            int s=utf[r*16+c];
            n+=(fm.width(s)-fm.leftBearing(s)-fm.rightBearing(s));
        }
    }
    n=(n*(symbol.bottom-symbol.top))/512;
    str+=tr("  size array = ")+QString::number(n)+tr(" kB");
	ui->label_font->setText(str);
}

void MainWindow::on_push_font_clicked(){
    fnt_d->setCurrentFont(fnt);
    fnt_d->show();
}

void MainWindow::on_sliderBottom_valueChanged(int value){
    symbol.bottom=symbol.height+value;
    if (symbol.bottom-symbol.top<1) symbol.top=symbol.bottom-1;
    ui->sliderTop->setValue(symbol.top);
    checkBorderline();
}

void MainWindow::on_sliderTop_valueChanged(int value){
    symbol.top=value;
    if (symbol.bottom-symbol.top<1) symbol.bottom=symbol.top+1;
    ui->sliderBottom->setValue(symbol.bottom-symbol.height);
    checkBorderline();
}

void MainWindow::checkBorderline(void){
    int code=symbol.code;
    for (int r=0;r<ui->table->rowCount();r++)
        for (int c=0;c<ui->table->columnCount();c++) {
            if(ui->table->item(r,c)->statusTip()=="yes"){
                symbol.code=utf[r*16+c];
                paintSymbol();
                if ((symbol.top>symbol.up)|(symbol.bottom<symbol.down)) ui->table->item(r,c)->setBackgroundColor(Qt::lightGray);
                else ui->table->item(r,c)->setBackgroundColor(Qt::white);
            }
            else ui->table->item(r,c)->setBackgroundColor(Qt::white);
        }
    symbol.code=code;
    ui->widget->update();
}

void MainWindow::on_actionSave_triggered(){
    on_push_save_clicked();
}

void MainWindow::on_actionChange_triggered(){
    on_push_font_clicked();
}

void MainWindow::on_actionExit_triggered(){
    exit(0);
}

void MainWindow::on_actionPen_triggered(){
on_push_pen_clicked();
}

void MainWindow::on_actionBrush_triggered(){
on_push_brush_clicked();
}


void MainWindow::pushButton_close_clicked(){
	frm->close();
}

void MainWindow::radioButton_0_clicked(){
    symbol.rotate=0;
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("symbol_rotate", symbol.rotate);
	set->setValue("gray_lcd", false);
	if (checkBox->isChecked()) format=QImage::Format_RGB32;
    ui->widget->update();
}

void MainWindow::radioButton_90_clicked(){
    symbol.rotate=90;
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("symbol_rotate", symbol.rotate);
	set->setValue("gray_lcd", false);
	if (checkBox->isChecked()) format=QImage::Format_RGB32;
    ui->widget->update();
}

void MainWindow::radioButton_180_clicked(){
    symbol.rotate=180;
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("symbol_rotate", symbol.rotate);
	set->setValue("gray_lcd", false);
	if (checkBox->isChecked()) format=QImage::Format_RGB32;
    ui->widget->update();
}

void MainWindow::radioButton_270_clicked(){
    symbol.rotate=270;
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("symbol_rotate", symbol.rotate);
	set->setValue("gray_lcd", false);
	if (checkBox->isChecked()) format=QImage::Format_RGB32;
    ui->widget->update();
}

void MainWindow::radioButton_gray_clicked(){
    symbol.rotate=0;
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("symbol_rotate", symbol.rotate);
	set->setValue("gray_lcd", true);
	if (checkBox->isChecked()) format=QImage::Format_RGB16;
    ui->widget->update();
}

void MainWindow::checkBox_toggled(bool checked){
    if(!checked) format=QImage::Format_Mono;
	else if (radioButton_gray->isChecked()) format=QImage::Format_RGB16;
    else format=QImage::Format_RGB32;
    ui->widget->update();
}

void MainWindow::Edit_type16_editingFinished(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("type16", Edit_type16->text());
	QString str=Edit_type16->text()+" font_";
    if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration->setText(str);
}

void MainWindow::Edit_type8_editingFinished(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("type8", Edit_type8->text());
	QString str=Edit_type16->text()+" font_";
    if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration->setText(str);
}


void MainWindow::check_arc_stateChanged(int state){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("arc", state);
	QString str=Edit_type16->text()+" font_";
    if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration->setText(str);
}

void MainWindow::check_color_stateChanged(int state){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("color", state);
	QString str=Edit_type16->text()+" font_";
	if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration->setText(str);
}

void MainWindow::check_width_stateChanged(int checked){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("width", checked);
}

void MainWindow::Edit_include_textChanged(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("include", Edit_include->toPlainText());
}

void MainWindow::Edit_attr_editingFinished(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setValue("attr", Edit_attr->text());
	QString str=Edit_type16->text()+" font_";
    if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration->setText(str);
}

void MainWindow::radioButton_lr_ud_clicked(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setValue("scan_lcd",radioButton_ud_lr->isChecked());
}

void MainWindow::radioButton_ud_lr_clicked(){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setValue("scan_lcd",radioButton_ud_lr->isChecked());
}

void MainWindow::on_actionConfigure_triggered(){

	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	frm = new QWidget();
	tabWidget = new QTabWidget();
	tabWidget->setObjectName(QStringLiteral("tabWidget"));
	tab = new QWidget();
	tab->setObjectName(QStringLiteral("tab"));
	checkBox = new QCheckBox(tr("Antialiasing"));
	checkBox->setObjectName(QStringLiteral("checkBox"));
	checkBox->setStyleSheet(QStringLiteral("border-width: 0px;"));
	checkBox->setChecked(true);
	connect(checkBox, SIGNAL(toggled(bool)),this, SLOT(checkBox_toggled(bool)));
	groupBox = new QGroupBox(tr("Switch orientation LCD display"));
	groupBox->setObjectName(QStringLiteral("groupBox"));
	groupBox->setFlat(false);
	groupBox->setCheckable(false);
	radioButton_gray = new QRadioButton(tr("excluding construction LCD"),groupBox);
	radioButton_gray->setObjectName(QStringLiteral("radioButton_gray"));
	radioButton_gray->setChecked(set->value("gray_lcd").toBool());
	connect(radioButton_gray, SIGNAL(clicked()),this, SLOT(radioButton_gray_clicked()));
	radioButton_0 = new QRadioButton(tr("do not turn"),groupBox);
	radioButton_0->setObjectName(QStringLiteral("radioButton_0"));
	if ((set->value("gray_lcd").toBool()==false)&&(set->value("symbol_rotate").toInt()==0)) radioButton_0->setChecked(true);
	connect(radioButton_0, SIGNAL(clicked()),this, SLOT(radioButton_0_clicked()));
	radioButton_90 = new QRadioButton(tr("rotated 90° clockwise"),groupBox);
	radioButton_90->setObjectName(QStringLiteral("radioButton_90"));
	if (set->value("symbol_rotate").toInt()==90) radioButton_90->setChecked(true);
	connect(radioButton_90, SIGNAL(clicked()),this, SLOT(radioButton_90_clicked()));
	radioButton_180 = new QRadioButton(tr("flip vertical"),groupBox);
	radioButton_180->setObjectName(QStringLiteral("radioButton_180"));
	if (set->value("symbol_rotate").toInt()==180) radioButton_180->setChecked(true);
	connect(radioButton_180, SIGNAL(clicked()),this, SLOT(radioButton_180_clicked()));
	radioButton_270 = new QRadioButton(tr("rotated 90° counter-clockwise"),groupBox);
	radioButton_270->setObjectName(QStringLiteral("radioButton_270"));
	if (set->value("symbol_rotate").toInt()==270) radioButton_270->setChecked(true);
	connect(radioButton_270, SIGNAL(clicked()),this, SLOT(radioButton_270_clicked()));
	QVBoxLayout* rLayout = new QVBoxLayout;
	rLayout->addWidget(radioButton_gray);
	rLayout->addWidget(radioButton_0);
	rLayout->addWidget(radioButton_90);
	rLayout->addWidget(radioButton_180);
	rLayout->addWidget(radioButton_270);
	groupBox->setLayout(rLayout);
	groupBox_2 = new QGroupBox(tr("Switching scan display"),tab);
	groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
	radioButton_lr_ud = new QRadioButton(groupBox_2);
	radioButton_lr_ud->setObjectName(QStringLiteral("radioButton_lr_ud"));
	QIcon icon7;
	icon7.addFile(QStringLiteral(":/pic/img/lr_ud.png"), QSize(), QIcon::Normal, QIcon::Off);
	radioButton_lr_ud->setIcon(icon7);
	radioButton_lr_ud->setIconSize(QSize(80, 100));
    radioButton_lr_ud->setChecked(!set->value("scan_lcd").toBool());
	connect(radioButton_lr_ud, SIGNAL(clicked()),this, SLOT(radioButton_lr_ud_clicked()));
	radioButton_ud_lr = new QRadioButton(groupBox_2);
	radioButton_ud_lr->setObjectName(QStringLiteral("radioButton_ud_lr"));
	QIcon icon8;
	icon8.addFile(QStringLiteral(":/pic/img/ud_lr.png"), QSize(), QIcon::Normal, QIcon::Off);
	radioButton_ud_lr->setIcon(icon8);
	radioButton_ud_lr->setIconSize(QSize(80, 100));
    radioButton_ud_lr->setChecked(set->value("scan_lcd").toBool());
	connect(radioButton_ud_lr, SIGNAL(clicked()),this, SLOT(radioButton_ud_lr_clicked()));
	QHBoxLayout* iLayout = new QHBoxLayout;
	iLayout->addWidget(radioButton_lr_ud);
	iLayout->addWidget(radioButton_ud_lr);
	groupBox_2->setLayout(iLayout);

	QVBoxLayout* mainLayout = new QVBoxLayout;
	QHBoxLayout* hLayout = new QHBoxLayout;
	QVBoxLayout* vLayout = new QVBoxLayout;
	QVBoxLayout* chLayout = new QVBoxLayout;
	chLayout->setContentsMargins(0,4,0,8);
	chLayout->addWidget(checkBox);
	vLayout->addLayout(chLayout);
	vLayout->addWidget(groupBox);
	hLayout->addLayout(vLayout);
	hLayout->addWidget(groupBox_2);
	tab->setLayout(hLayout);
	tabWidget->addTab(tab,tr("settings LCD"));

	tab_2 = new QWidget();
	tab_2->setObjectName(QStringLiteral("tab_2"));
	QLabel* label_5 = new QLabel(tr("include files:"),tab_2);
	Edit_attr = new QLineEdit(set->value("attr").toString(),tab_2);
	connect(Edit_attr, SIGNAL(editingFinished()),this, SLOT(Edit_attr_editingFinished()));
	Edit_include = new QTextEdit();
	Edit_include->setMinimumHeight(100);
	Edit_include->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
	Edit_include->setFixedHeight(Edit_attr->sizeHint().height()*2);
	Edit_include->setText(set->value("include").toString());
	connect(Edit_include, SIGNAL(textChanged()),this, SLOT(Edit_include_textChanged()));
	QLabel* label_6 = new QLabel(tr("16 bits type declaration constant:"),tab_2);
	Edit_type16 = new QLineEdit(set->value("type16").toString(),tab_2);
	connect(Edit_type16, SIGNAL(editingFinished()),this, SLOT(Edit_type16_editingFinished()));
	QLabel* label_7 = new QLabel(tr("8 bits type declaration constant:"),tab_2);
	Edit_type8 = new QLineEdit(set->value("type8").toString(),tab_2);
	connect(Edit_type8, SIGNAL(editingFinished()),this, SLOT(Edit_type8_editingFinished()));
	QLabel *label_8 = new QLabel(tr("if necessary attributes:"),tab_2);
	check_arc = new QCheckBox(tr("Compress data"),tab_2);
	check_arc->setChecked(set->value("arc").toInt());
	connect(check_arc, SIGNAL(stateChanged(int)),this, SLOT(check_arc_stateChanged(int)));
	check_width = new QCheckBox(tr("Table with symbol sizes"),tab_2);
	check_width->setChecked(set->value("width").toInt());
	connect(check_width, SIGNAL(stateChanged(int)),this, SLOT(check_width_stateChanged(int)));
	check_color = new QCheckBox(tr("Add color name"),tab_2);
	check_color->setChecked(set->value("color").toInt());
	connect(check_color, SIGNAL(stateChanged(int)),this, SLOT(check_color_stateChanged(int)));
	QString str=Edit_type16->text()+" font_";
    if (set->value("arc").toInt()==Qt::Checked)str=Edit_type8->text()+" font_";
	str+=nameFont();
	str+="[] "+Edit_attr->text()+"={";
	label_declaration = new QLabel(str,tab_2);
	QFont font;
	font.setBold(true);
	label_declaration->setFont(font);
	label_declaration->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
	label_declaration->setWordWrap(true);
	label_declaration->setTextInteractionFlags(Qt::TextEditorInteraction);
	label_declaration->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

	QVBoxLayout* main1Layout = new QVBoxLayout;
	QFormLayout* fLayout = new QFormLayout;
	QVBoxLayout* h1Layout = new QVBoxLayout;
	QHBoxLayout* cLayout = new QHBoxLayout;
	QVBoxLayout* dLayout = new QVBoxLayout;
	fLayout->addRow(label_5,Edit_include);
	fLayout->addRow(label_6,Edit_type16);
	fLayout->addRow(label_7,Edit_type8);
	fLayout->addRow(label_8,Edit_attr);
	cLayout->setContentsMargins(0,6,0,8);
	cLayout->addWidget(check_arc);
	cLayout->addWidget(check_width);
	cLayout->addWidget(check_color);
	h1Layout->addLayout(fLayout);
	h1Layout->addLayout(cLayout);
	dLayout->addWidget(label_declaration);
	h1Layout->addLayout(dLayout);
	main1Layout->addLayout(h1Layout);

	tab_2->setLayout(main1Layout);
	tabWidget->addTab(tab_2,"settings files .c");
	mainLayout->addWidget(tabWidget);
	pushButton_close=new QPushButton(tr("close"));
	connect(pushButton_close, SIGNAL(clicked()),this, SLOT(pushButton_close_clicked()));
	mainLayout->addWidget(pushButton_close);

	frm->setWindowTitle(tr("settings"));
	frm->setLayout(mainLayout);

	frm->setAttribute(Qt::WA_DeleteOnClose);
	frm->setWindowFlags(Qt::Dialog);
	frm->setWindowModality(Qt::WindowModal);
	frm->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
	frm->show();
}

void MainWindow::on_push_setting_clicked()
{
    on_actionConfigure_triggered();
}

void MainWindow::Calc()
{
    QFontMetrics fm(fnt);
    int a=0;
    a-=fm.leftBearing(symbol.code);
    a+=fm.width(symbol.code)+3;
    a-=fm.rightBearing(symbol.code);
    int b=fm.ascent()+fm.descent()+3;
    if ((a>0)&&(b>0))
    {
        if (symbol.code==0x20) a=b/3;
        symbol.x=-fm.leftBearing(symbol.code)+1;
        symbol.y=fm.ascent();
        symbol.width=a;
        symbol.height=b;
        symbol.k=(ui->widget->width()-2)/a;
        if (symbol.k>(ui->widget->height()-2)/b) symbol.k=(ui->widget->height()-2)/b;
    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *evt) {
    if (evt->type() == QEvent::Paint) {
        paintSymbol();
        QPainter paint(ui->widget);
        paint.setPen(Qt::NoPen);
        paint.drawRect(0,0,ui->widget->width(),ui->widget->height());
        paint.drawImage(1,1,img->scaled(symbol.width*symbol.k,symbol.height*symbol.k,Qt::KeepAspectRatio,Qt::FastTransformation));
        paint.setPen(Qt::red);
        paint.drawLine(1,symbol.top*symbol.k+1,symbol.width*symbol.k+1,symbol.top*symbol.k+1);
        paint.drawLine(1,symbol.bottom*symbol.k+1,symbol.width*symbol.k+1,symbol.bottom*symbol.k+1);
        return true;
    }
    return QMainWindow::eventFilter( obj, evt );
}

QString MainWindow::nameFont(void){
	QSettings *set = new QSettings("settings.ini",QSettings::IniFormat);
	QString str=fnt.family().replace(' ','_')+"_"+QString::number(symbol.bottom-symbol.top);
	if (fnt.bold()) str+="_b";
	if (fnt.italic()) str+="_i";
	if (set->value("color").toInt()==Qt::Checked){
		str+=QString("_%1%2%3_%4%5%6")
				.arg(QString::number(symbol.pen.red()/16,16).toUpper())
				.arg(QString::number(symbol.pen.green()/16,16).toUpper())
				.arg(QString::number(symbol.pen.blue()/16,16).toUpper())
				.arg(QString::number(symbol.brush.red()/16,16).toUpper())
				.arg(QString::number(symbol.brush.green()/16,16).toUpper())
				.arg(QString::number(symbol.brush.blue()/16,16).toUpper());
	}
	return str;
}

void MainWindow::paintSymbol(void){
    Calc();
    QPainter paint;
	int s_width=symbol.width;
	int s_height=symbol.height;
    if ((symbol.rotate==90)|(symbol.rotate==270)){
        s_width=symbol.height;
        s_height=symbol.width;
    }
    QImage ims(symbol.height,symbol.height,format);
    paint.begin(&ims);
    paint.initFrom(this);
    paint.setFont(fnt);
    paint.setBrush(QBrush(symbol.brush,Qt::SolidPattern));
    paint.setPen(QPen(symbol.pen,1,Qt::SolidLine,Qt::FlatCap));
    paint.fillRect(0,0,symbol.height,symbol.height,QBrush(symbol.brush,Qt::SolidPattern));
    if (symbol.code!=0x20){
        // для пробела это не делается
        QPointF center(symbol.height/2.0,symbol.height/2.0);
        paint.translate(center);
        paint.rotate(symbol.rotate);
        paint.translate(-center);
        paint.drawText(symbol.x,symbol.y,QString(symbol.code));
        paint.end();
        QImage im(symbol.width,symbol.height,format);
        paint.begin(&im);
        paint.initFrom(this);
        paint.setFont(fnt);
        paint.setBrush(QBrush(symbol.brush,Qt::SolidPattern));
        paint.setPen(QPen(symbol.pen,1,Qt::SolidLine,Qt::FlatCap));
        paint.fillRect(0,0,symbol.width,symbol.height,QBrush(symbol.brush,Qt::SolidPattern));
        paint.translate(center);
        paint.rotate(-symbol.rotate);
        paint.translate(-center);
        paint.drawImage(0,0,ims);
        paint.end();
        QList<int> up;
        QList<int> down;
        for (int x=0;x<symbol.width;x++){
            int y=0;
            int max=symbol.height;
            while(y<max){
                if (im.pixel(x,y)!=symbol.brush.rgb()) break;
                y++;
            }
            up.append(y);
            while(max>y){
                if (im.pixel(x,max-1)!=symbol.brush.rgb()) break;
                max--;
            }
            down.append(max);
        }
        symbol.up=symbol.height;
        symbol.down=0;
        int min=0;
        int max=symbol.width;
        bool flag=true;
        for (int x=0;x<symbol.width;x++){
            if (up.at(x)<symbol.height){
                flag=false;
                if (symbol.down<down.at(x)) symbol.down=down.at(x);
                if (symbol.up>up.at(x)) symbol.up=up.at(x);
                max=x;
            }
            else if (flag) min=x+1;
        }
        symbol.width=max-min+1;
        symbol.x=symbol.x+min;
        img=new QImage(symbol.width,symbol.height,format);
        *img=im.copy(min,0,symbol.width,symbol.height);
    }
    else{
        paint.end();
        img=new QImage(symbol.width,symbol.height,format);
        *img=ims.copy(0,0,symbol.width,symbol.height);
    }
	ui->label_w->setText(tr("character width=%1")
			.arg(symbol.width));
	ui->label_h->setText(tr("font height=%1")
			.arg(symbol.height));
	ui->label_xy->setText(tr("height to record=%1")
			.arg(symbol.bottom-symbol.top));
}

/// заполнение словаря
void MainWindow::fillDictionary()
{
	QHash<quint16,quint32> table;
    // хеш заполняется уникальными значениями и количеством их повторов.
    for (int i=0;i<font_.size();i++) table.insert(font_[i],(table.value(font_[i],0))+1);
    // в цикле заполняется массив dictionary DICT самых встречаемых значений.
    for (int i=0;i<DICT;i++){
		quint32 ind=0;
		QHash<quint16,quint32>::iterator k;
		QHash<quint16,quint32>::iterator n_iterator = table.begin();
        n_iterator=table.begin();
        while(n_iterator!=table.constEnd()){
            if (ind<n_iterator.value()){
                ind=n_iterator.value();
                k=n_iterator;
            }
            ++n_iterator;
        }
        dictionary[i]=k.key();
        table.erase(k);
        if (table.isEmpty()) break;
    }
}

/// процесс архивирования
void MainWindow::fillArc(int bias,int count)
{
    quint8 b;
    quint8 co=0;
	quint16 val=font_[bias];
	quint16 n=val;
    for (int x=bias+1;x<count+bias;x++){
        n=font_[x];
        b=DICT+1;
        for (int i=0;i<DICT;i++) if (dictionary[i]==val){b=i; break;}
        if (n!=val){ // записываем предыдущий байт
            if (b<DICT) dest.append(co*64+b+32);// есть в словаре
            else { // нет в словаре
				dest.append((quint8)val & 0xDF);
				dest.append((quint8)(val/256));
            }
            val=n;
            co=0;
        }
        else{ // совпадает с предыдущим
            if (b<DICT){ // есть в словаре
                co++;
                if (co>3){
                    dest.append(0xE0+b);
                    co=0;
                }
            }
            else{
                // нет в словаре
				dest.append((quint8)val & 0xDF);
				dest.append((quint8)(val/256));
                co=0;
            }
        }
    }
    // записывается последний пиксель
    if (co==0){
        b=DICT+1;
        for (int i=0;i<DICT;i++) if (dictionary[i]==n){b=i; break;}
        if (b<DICT) dest.append(b+32);// есть в словаре
        else { // нет в словаре
			dest.append((quint8)n & 0xDF);
			dest.append((quint8)(n/256));
        }
    }
    else dest.append(co*64+b+32); // если осталась очередь
}
