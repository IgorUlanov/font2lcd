<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="363"/>
        <source>font</source>
        <translation>шрифт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="376"/>
        <source>font color</source>
        <translation>цвет шрифта</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="389"/>
        <source>background color</source>
        <translation>цвет фона</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="350"/>
        <source>save</source>
        <translation>запись</translation>
    </message>
    <message>
        <source>Save the file</source>
        <translation type="obsolete">Запись файла</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>upper limit</source>
        <translation>ограничение сверху</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <source>lower limit</source>
        <translation>ограничение снизу</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="621"/>
        <source>settings LCD</source>
        <translation>настройки ЖК-дисплея</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>Antialiasing</source>
        <translation>Сглаживание</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="556"/>
        <source>Switch orientation LCD display</source>
        <translation>Переключение положения ЖK</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="560"/>
        <source>excluding construction LCD</source>
        <translation>не учитывать конструкцию ЖК</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="564"/>
        <source>do not turn</source>
        <translation>без поворота</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>rotated 90° clockwise</source>
        <translation>по часовой стрелке на 90°</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="572"/>
        <source>flip vertical</source>
        <translation>отражение по вертикали</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <source>rotated 90° counter-clockwise</source>
        <translation>против часов стрелки на 90°</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="587"/>
        <source>Switching scan display</source>
        <translation>Переключение способа сканирования</translation>
    </message>
    <message>
        <source>settings files c</source>
        <translation type="vanished">настройки выходных файлов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="625"/>
        <source>include files:</source>
        <translation>подключаемые файлы:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="634"/>
        <source>16 bits type declaration constant:</source>
        <oldsource>type declaration constant:</oldsource>
        <translation>объявление 16-ти битной константы:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="640"/>
        <source>if necessary attributes:</source>
        <translation>необходимые атрибуты:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="641"/>
        <source>Compress data</source>
        <translation>Сжать данные</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>8 bits type declaration constant:</source>
        <translation>объявление 8-и битной константы:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="685"/>
        <source>close</source>
        <translation>закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="47"/>
        <source>w</source>
        <translation>w</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="60"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="402"/>
        <location filename="mainwindow.cpp" line="689"/>
        <source>settings</source>
        <translation>настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="472"/>
        <source>Save</source>
        <translation>Запись</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="484"/>
        <source>Change Fonts</source>
        <translation>Изменить шрифт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>Configure</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="508"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="520"/>
        <source>Change color pen</source>
        <translation>Изменить цвет шрифта</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="532"/>
        <source>Change color brush</source>
        <translation>Изменить цвет фона</translation>
    </message>
    <message>
        <source>character width=%1%4font height=%2%4height to record=%3</source>
        <oldsource>character width=%1
font height=%2
height to record=%3</oldsource>
        <translation type="vanished">ширина символа=%1%4высота шрифта=%2%4высота для записи=%3</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="152"/>
        <source>save files: %1</source>
        <translation>запись файла: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <source>// the size of an array of characters = %1 Kb%2</source>
        <translation>// размер массива символов = %1 Кб%2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="287"/>
        <source>use simbol(s)</source>
        <translation>использовать символ(ы)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="288"/>
        <source>no use symbol(s)</source>
        <translation>не использовать символ(ы)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>  size array = </source>
        <translation>  размер массива = </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source> kB</source>
        <translation>кБ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="644"/>
        <source>Table with symbol sizes</source>
        <oldsource>Table with symbol size</oldsource>
        <translation>Таблица с размерами символов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="647"/>
        <source>Add color name</source>
        <translation>Добавить цвет к имени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="832"/>
        <source>character width=%1</source>
        <translation>ширина символа=%1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="834"/>
        <source>font height=%1</source>
        <translation>высота шрифта=%1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="836"/>
        <source>height to record=%1</source>
        <translation>высота при записи=%1</translation>
    </message>
    <message>
        <source>Use</source>
        <translation type="obsolete">использовать символ(ы)</translation>
    </message>
    <message>
        <source>No use</source>
        <translation type="obsolete">не использовать символ(ы)</translation>
    </message>
</context>
</TS>
