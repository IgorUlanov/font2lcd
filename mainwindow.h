#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QString>
#include <QFont>
#include <QFontDialog>
#include <QFontMetrics>
#include <QTextStream>
#include <QFile>
#include <QCheckBox>
#include <QSettings>
#include <QGroupBox>
#include <QRadioButton>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>
#include <QThread>

#define DICT 32

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_table_cellClicked(int row, int column);
    void on_push_pen_clicked();
    void on_push_brush_clicked();
    void on_push_save_clicked();
    void on_table_customContextMenuRequested(const QPoint &pos);
    void fontUpdate(const QFont &);
    void nouseSymbols(void);
    void useSymbols(void);
    void on_push_font_clicked();
    void on_sliderBottom_valueChanged(int value);
    void on_sliderTop_valueChanged(int value);
    void on_actionSave_triggered();
    void on_actionChange_triggered();
    void on_actionConfigure_triggered();
    void on_actionExit_triggered();
    void on_actionPen_triggered();
    void on_actionBrush_triggered();
	void pushButton_close_clicked();
	void radioButton_0_clicked();
	void radioButton_90_clicked();
	void radioButton_180_clicked();
	void radioButton_270_clicked();
	void radioButton_gray_clicked();
	void checkBox_toggled(bool checked);
	void Edit_attr_editingFinished();
	void Edit_include_textChanged();
	void on_push_setting_clicked();
	void Edit_type16_editingFinished();
	void Edit_type8_editingFinished();
	void check_arc_stateChanged(int);
	void check_width_stateChanged(int);
	void check_color_stateChanged(int);
	void radioButton_lr_ud_clicked();
	void radioButton_ud_lr_clicked();

private:
    Ui::MainWindow *ui;
	void saveFont();
    bool eventFilter(QObject *target, QEvent *event);
    void appendSymbol(void); // добавление символа в массив
    void fillTable(void);
    void saveSymbol(int,int); // запись одного символа
    void checkBorderline(void);
    void showFont(void); // вывод информации о шрифте
    void paintSymbol(void);
    void Calc(void);
    /// заполнение словаря
    void fillDictionary();
    /// процесс архивирования
    void fillArc(int bias,int count);
	QString nameFont(void);
    QFont fnt;
    QFontDialog* fnt_d;
    QImage *img;
    QList<QString> first,second;
	QList<quint16> font_,map_;
	QList<quint8> dest,width_;
	quint16 dictionary[DICT];
    QString home_path;
    int bias;
    quint16 utf[256]; // таблица соответствия кодов символов utf8 - cp1251
    QImage::Format format; // формат qimage
	QWidget *frm;
	QTabWidget *tabWidget;
	QWidget *tab;
	QCheckBox *checkBox;
	QGroupBox *groupBox;
	QRadioButton *radioButton_gray;
	QRadioButton *radioButton_0;
	QRadioButton *radioButton_90;
	QRadioButton *radioButton_180;
	QRadioButton *radioButton_270;
	QGroupBox *groupBox_2;
	QRadioButton *radioButton_lr_ud;
	QRadioButton *radioButton_ud_lr;
	QTextEdit *Edit_include;
	QWidget *tab_2;
	QLineEdit *Edit_type16;
	QLineEdit *Edit_type8;
	QLineEdit *Edit_attr;
	QCheckBox *check_arc;
	QCheckBox *check_width;
	QCheckBox *check_color;
	QPushButton *pushButton_close;
	QLabel* label_declaration;
	struct{
        int top; // верхняя граница
        int bottom; // нижняя граница
        int x; // начальная точка
        int y; // начальная точка
        int width; // ширина символа
        int height; // высота символа
        int up; // верхний использованный пиксель
        int down; // нижний использованный пиксель
        int code; // код символа
        int k; // коэффициент маштабирования символа при выводе на экран
        int rotate; // поворот в градусах
        QColor pen; // цвет символа
        QColor brush; // цвет фона
    }symbol;
};


#endif // MAINWINDOW_H
