set qt=F:\Qt\5.5.0\5.5\mingw492_32\bin\

copy /Y "%qt%libstdc++-6.dll" ..\release\
copy /Y "%qt%libgcc_s_dw2-1.dll" ..\release\
copy /Y "%qt%libwinpthread-1.dll" ..\release\
copy /Y ..\symbol.csv ..\release\
copy /Y ..\settings.ini ..\release\
copy /Y ..\font2lcd_ru.qm ..\release\

windeployqt.exe --dir ..\release\ --release --no-translations --no-system-d3d-compiler --no-quick-import ..\release\Font2lcd.exe
